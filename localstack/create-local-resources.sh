
awslocal ssm put-parameter \
    --name /nots/param/page-size \
    --type String \
    --overwrite \
    --region eu-west-2 \
    --value 100

awslocal ssm get-parameter --name "/nots/param/page-size" --region eu-west-2