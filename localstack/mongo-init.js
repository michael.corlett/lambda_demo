// db.createUser({
//     user: 'root',
//     pwd: 'example',
//     roles: [
//         {
//             role: 'readWrite',
//             db: 'transaction-logging-db',
//         },
//     ],
// });

// db = new Mongo().getDB('transaction-logging-db');

// db.createCollection('transaction-log', { capped: false });

// db['transaction-log'].insert(
//     {"agentEmailAddress": "ritesh.ranjan@dwp.gov.uk",
//     "notificationUID": "c699fd34-20e7-26ca-e328-05956a6fa0e5",
//     "transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e5",
//     "mobileNumber": "+447678653546",
//     "templateName": "ServiceNow1",
//     "organisationUnit": "Disability Living Allowance",
//     "businessUnit": "Child DRS",
//     "bulkUploadId": "",
//     "senderId": "DWP",
//     "message": "Your appointment is scheduled for 28-11-2021",
//     "state": "delivered",
//     "statusDescription": "Your message has been delivered",
//     "requestedDateTime":"2021-10-12T16:55:28.682Z",
//     "sendDateTime": "7532-02-29T14:33:43-06:42",
//     "completionDateTime": "7532-02-29T14:33:43-06:42"});

// db['transaction-log'].insert({
//     "agentEmailAddress": "thomas.edison@dwp.gov.uk",
//     "transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e8",
//     "mobileNumber": "7678653546",
//     "countryCode": 44,
//     "bulkUploadId": "k699fd34-0e7e-26ca-e328-05347e4r327",
//     "language": "en",
//     "organisationUnit": "Disability Living Allowance",
//     "businessUnit": "Child DRS",
//     "templateName": "ServiceNow",
//     "templateVersion": "1",
//     "senderId": "DWP",
//     "message": "Your appointment is scheduled for 28-11-2021",
//     "role": "Send SMS",
//     "subscriberName": "TMA-APP",
//     "requestedDateTime": "7532-02-29T14:33:43-06:42",
//     "state": "in-progress",
//     "statusCode": "00",
//     "statusDescription": "Your message is in progress"

// });

// db['transaction-log'].insert({
//     "agentEmailAddress": "thomas.edison@dwp.gov.uk",
//     "transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e8",
//     "mobileNumber": "7678653547",
//     "countryCode": 44,
//     "bulkUploadId": "k699fd34-0e7e-26ca-e328-05347e4r327",
//     "language": "en",
//     "organisationUnit": "Disability Living Allowance",
//     "businessUnit": "Child DRS",
//     "templateName": "ServiceNow",
//     "templateVersion": "1",
//     "senderId": "DWP",
//     "message": "Your appointment is scheduled for 29-11-2021",
//     "role": "Send SMS",
//     "subscriberName": "TMA-APP",
//     "requestedDateTime": "7532-02-29T14:33:43-06:42",
//     "state": "in-progress",
//     "statusCode": "00",
//     "statusDescription": "Your message is in progress"

// });

// db['transaction-log'].insert({"agentEmailAddress": "ritesh.ranjan@dwp.gov.uk","notificationUID": "c699fd34-20e7-26ca-e328-05956a6fa0e6","transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e6","mobileNumber": "7678653546","templateName": "ServiceNow2","organisationUnit": "Disability Living Allowance","businessUnit": "Child DRS","bulkUploadId": "k699fd34-0e7e-26ca-e328-05347e4r327","senderId": "DWP","message": "Your appointment is scheduled for 28-11-2021","state": "delivered","statusDescription": "Your message has been delivered","requestedDateTime":"2021-06-30T16:55:28.682Z", "sendDateTime": "7532-02-29T14:33:43-06:42","completionDateTime": "7532-02-29T14:33:43-06:42"});

// db['transaction-log'].insert({"agentEmailAddress": "ritesh.ranjan@dwp.gov.uk","notificationUID": "c699fd34-20e7-26ca-e328-05956a6fa0e7","transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e7","mobileNumber": "7678653546","templateName": "ServiceNow3","organisationUnit": "Disability Living Allowance","businessUnit": "Child DRS","bulkUploadId": "k699fd34-0e7e-26ca-e328-05347e4r327","senderId": "DWP","message": "Your appointment is scheduled for 28-11-2021","state": "delivered","statusDescription": "Your message has been delivered","requestedDateTime":"2021-10-06T16:55:28.682Z", "sendDateTime": "7532-02-29T14:33:43-06:42","completionDateTime": "7532-02-29T14:33:43-06:42"});

// db['transaction-log'].insert({"agentEmailAddress": "ritesh.ranjan@dwp.gov.uk","notificationUID": "c699fd34-20e7-26ca-e328-05956a6fa0e8","transactionId": "c699fd34-20e7-26ca-e378-05247w3fa5e8","mobileNumber": "7678653546","templateName": "ServiceNow4","organisationUnit": "Disability Living Allowance","businessUnit": "Child DRS","bulkUploadId": "k699fd34-0e7e-26ca-e328-05347e4r327","senderId": "DWP","message": "Your appointment is scheduled for 28-11-2021","state": "failed","statusDescription": "Your message has been delivered","requestedDateTime":"2021-10-05T16:55:28.682Z", "sendDateTime": "7532-02-29T14:33:43-06:42","completionDateTime": "7532-02-29T14:33:43-06:42"});


// db['transaction-log'].insert({"transactionId":"a8b7b360-7513-5028-8302-96140c7add3e","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","message":"8b9cfecb9c92d740e3d48b0ffdbb3f4b90da34bb211e8eec1271dfe87aad359c64e22c9b3e21effe3e1a6379a0a4eed7a9946d22fae9bebe94fe2f8f9d2551ba6d43bf9dfe64451273aea8e0fa11c2ba7959278f431643af867bd9abfdd1171f4d9d715e8b202237c3ccdf9cc3989ea8b25eb689122e5c15a11bbf0d1e","mobileNumber":"d7e374d1dc368536ea6953b422ec1b9f96c9891bd9825ce46da6398f08f5138886502bcb2df19b0840217d8985","mobileNumberBIDX":"e936","requestedDateTime":"2021-08-08T00:00:00.000Z","senderId":"WrTRVUer)A","state":"In-Progress","templateName":"Transformation Top Lambda"});

// db['transaction-log'].insert({"transactionId":"8fc3c9d3-938f-5fd9-a660-c38decd55eb2","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","message":"e8ac85962d3837d265d47b192085ec5ca1ce89702a2f06d29b9e405d9369ca9cbbd8e965b908b4933f6c493d2fe1102101976d63b402ba47248397c851a1cdae79a6d7cbab872755e8f166ea61437c7036d114c92dc9010a09a6aa463b86c938ea6f86cdc72b24c5cfe6b0d16e06d9400dc98a26e2aceae6216cdb612b","mobileNumber":"5de83b28ec1d7e15bb10560778c88c157fda733167fcf35601e619b6fe7d8e8f059a123741f58b8f099897c2e0","mobileNumberBIDX":"e936","requestedDateTime":"2021-08-08T00:00:00.000Z","senderId":"WrTRVUer)A","state":"In-Progress","templateName":"Transfromation Bottom Lambda","language":"tr","notificationUID":"3999fb1e-93e7-5118-b855-ffc596453fd8","subscriberName":"ZS)zuzV9^u737BghNDF"});

// db['transaction-log'].insert({"transactionId":"fbc311ab-7069-581b-8938-1637b5e989d1","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","message":"5cc4499d14fb7dae3488ef53b094b519b033ac9f2406ab7032db5ba28cab412516891f908bc900e604822eab1bf6b342159c8f56118e5cfb68fc02b2a80378eaf11f7a8dd4da2e0484d41b1fc8199443a8c8a76138fe9c27d13c8150ea66f330a30920bfa4a26630572f9bd23202206ea8ba647d20bd72b5dbb4441a16","mobileNumber":"5f0bc39436c4cef7fe6b2aae70746a236aae795d2a5c0c29b57447fc293ee464aba4c96ad051c11d648ac8f81d","mobileNumberBIDX":"e936","requestedDateTime":"2021-08-08T00:00:00.000Z","senderId":"WrTRVUer)A","state":"In-Progress","templateName":"Channel Adapter Lambda","language":"tr","notificationUID":"827f0c5d-dd59-55d4-bb13-13c4ad11c015","subscriberName":"ZS)zuzV9^u737BghNDF","fragments":1,"sendDateTime":"2021-08-09T00:00:00.000Z"});

// db['transaction-log'].insert({"transactionId":"9edfec35-4a1f-51a2-9eb5-c4a3e9db9353","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","message":"3d5014d916f94f8ca8785d1c5c5dd3e723914b800c22629a4174925333f7929f642931f6d4677d81f9fc422aa80f613f952ef3f7291747452f968a4082373b75e4fc7ede22b5d9e2b673f0980b7cc70d117c32c463daa8b0118429389ab0a01936e915f0adc96cc43dfcbaf593ac1bca95f77d7196c25bfb2e4dcf390b","mobileNumber":"0488d693d5c19d5141f5732518ffd9e43ead2e72fc498f9c9f59a4758dad35073ddd97ca02099d3e4e9ca6c1f8","mobileNumberBIDX":"e936","requestedDateTime":"2021-08-08T00:00:00.000Z","senderId":"WrTRVUer)A","state":"delivered","templateName":"Callback Lambda","language":"tr","notificationUID":"9c584028-e7f5-554a-8f47-f1dd0ff98272","subscriberName":"ZS)zuzV9^u737BghNDF","fragments":1,"sendDateTime":"2021-08-09T00:00:00.000Z","completionDateTime":"2021-08-09T00:00:00.000Z","notify":{"id":"86dc00da-5f38-5326-b4ef-b02f30a94e3d","createdDateTime":"2021-08-09T00:00:00.000Z","sendDateTime":"2021-08-09T00:00:00.000Z","notificationType":"twhZMBFmKGy8y","templateId":"PCL80I","templateVersion":-4310283128406016}});

// db['transaction-log'].insert({"transactionId":"3ca2110e-cc3a-52e4-9dca-c6a586f415a5","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","language":"mn","message":"141c60792ce270a9b7b63b2b439a3303133036816185f13607296e403bd622c6b934499f244c140bc422452a1969fbe6d3677967c5e3ea22b68da7b5b3fa66afd7b3f42973627c6811db3ffd58a8f83f470f8c97647698d83718d46ee0f5831751c04c30eeface46bdc7970d24d4dc695e86699fa5c021aeb38a3b0e93","mobileNumber":"bca34d69aa1dd55b2dc6e793ab774fca15971961f4bd3e342c10a7856fc6f81e9226f4ff4483c69248e8f92d97","mobileNumberBIDX":"e936","notificationUID":"5c2f9f5a-621f-53b4-b8db-7498646c8a96","requestedDateTime":"2021-08-08T00:00:00.000Z","state":"In-Progress","subscriberName":"wlZ[z","templateName":"SMS Notification Lambda from TMA APP"});

// db['transaction-log'].insert({"transactionId":"f26c0af2-c739-5c26-867f-0823f903f02c","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","language":"mn","message":"2c415336997925d56681781c02cd1a578ae897b52073d0df5d41a74f91e42410f41fa306b1a35793c29a4499d03ef87f78fa7e2b63d145635c0edbb1b2ba7e89a1a71b9e65e1badf2fa04e5ef3f7025228b08e7dbdf2cdb6dd843817b8af78e5b631ef8e04fc34ea8d441f47a014041927ef088bae447a642b5cb66e0ea0","mobileNumber":"9f3be1424d8787edddfd99fb322d88a1b4edd1b6e9f4ab15b901df8c92a9138c2a0d24ff2388887a9b365bc12b","mobileNumberBIDX":"e936","notificationUID":"fdd1860a-abe0-5918-8385-952d9981313d","requestedDateTime":"2021-08-08T00:00:00.000Z","state":"In-Progress","subscriberName":"wlZ[z","templateName":"Channel Adapter Lambda from TMA APP","fragments":1,"sendDateTime":"2021-08-09T00:00:00.000Z"});

// db['transaction-log'].insert({"transactionId":"cf6fdd10-9c83-5e53-9ff0-e0428595fba7","agentEmailAddress":"thomas.edison@dwp.gov.uk","countryCode":"44","language":"mn","message":"3bb42da65f7c8d7d70d7828ec45f67ee688e65cd601d53cc07b57a60fefbbdbdd21700649f1768c5887a00340e1abf0e9fdb51365b7c289e3336043bab74c39cc4672e31ab99edd777e856ee2d72a92dc0c0097fad453bc1976750b75c62bbb7b96f70fc0039f2253ffcb20564643a30fc26b299f5546d2472471348e1","mobileNumber":"d33fce5a2ccf987ff72bb484f2d36b30e995b36b503f3aae99132e07b56791b7f8a1d41bbe72ae17d6eb593226","mobileNumberBIDX":"e936","notificationUID":"f3bd1da9-d31f-5132-bfe8-b42610109137","requestedDateTime":"2021-08-08T00:00:00.000Z","state":"delivered","subscriberName":"wlZ[z","templateName":"Callback API Lambda from TMA APP","fragments":1,"sendDateTime":"2021-08-09T00:00:00.000Z","completionDateTime":"2021-08-09T00:00:00.000Z","notify":{"id":"a49238c3-015c-59f5-bd25-30f96c3c5e58","createdDateTime":"2021-08-09T00:00:00.000Z","sendDateTime":"2021-08-09T00:00:00.000Z","notificationType":"GJfgQMr*","templateId":"@J9zR","templateVersion":-7964205680427008}});
