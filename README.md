# Lambda Demo

## SAM projects

There are two SAM projects in this repo, demo1 and demo2. 

### Demo1
is just a lambda with an API integration. The code was stolen from somewhere else
but the code isn't the important thing.

Note the package.json does not include the AWS SDK - it comes with the lambda environment so I don't add it to keep the code size down. If the code size is too big, you can't edit it in the console.

Files

- template.yaml is the SAM definition file, it creates the lambda and the API gateway - implicitly as the source of the event for the lambda.
- samconfig.toml is my configuration file, which won't work for you as it references an S3 bucket (SAM's state repository) you don't have. You should do 
``sam deploy -guided`` to set up your deployment in your environment.
- bd_dev.sh just does an npm prune to remove non-prod dependencies, if you are using npm install to build the code locally (you don't need to) 
- startapp.sh is commonly found in NOTS projects which use SAM for local development. It starts the API running in the local environment with ``sam local start-api`` and passes environment variables in *env.json*
- create_local.sh is an experimental script which deploys the lambda *directly into localstack*, where it can be invoked directly. Note the number of steps required for creating an API gateway.
There are some weirdnesses to resolve, like the path used for the api needing to contain __user_request__ for some reason, like
``http://localhost:4566/restapis/n19nmfmdub/test/_user_request_/abc``

### localstack

The localstack folder contains *docker-compose.yml* and supporting files to deploy localstack to a container. Note the services enabled (line 22) include ``lambda`` which supports the creation of lambdas in localstack described above.

### demo2

Demo2 is demo1 extended to include an SQS queue to reflect our pattern 2 (top/bottom) lambda pattern. The "top" lambda simply puts the payload on the queue and the bottom lambda reads from the SQS event.

Note I cheated here; the template creates the queue ``demo2_queue`` and sets it as the event to the bottom lambda and passes its ARN to the top lambda; however the top lambda has no permissions to write to the queue without adding ``AmazonSQSFullAccess`` as a policy for the top lambda. This gives it blanket SQS permissions, which is clearly not desirable from a security point of view. Fine for demos though.

Also there's no create_local.sh for demo2, I didn't get around to creating the queue and wiring it up in localstack. An exercise for the reader.

