npm prune -prod
zip -r api.zip src/*.js node_modules/
aws lambda update-function-code --region eu-west-2 --function-name demo_api_top --zip-file fileb://api.zip
aws lambda update-function-code --region eu-west-2 --function-name demo_api_bottom --zip-file fileb://api.zip
rm api.zip
