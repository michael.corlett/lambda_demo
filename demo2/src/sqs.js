const AWS = require('aws-sdk');

let sqs = null;
let url_cache = {};

function sqs_configure() {
    if( !sqs ) {
        let aws_options = { endpoint : process.env.AWS_SQS_ENDPOINT};
        sqs = new AWS.SQS(aws_options);
    }
}

async function url_for_queue( qName ) {

    if( url_cache.hasOwnProperty(qName) ) {
        return url_cache[qName];
    }

    sqs_configure();

    let url = null;

    await sqs.getQueueUrl( {"QueueName" : qName } ).promise()
    .then(function(data) {
        url = data.QueueUrl;
    }).catch(function(err) {
        console.log(err);
        console.error(err.message, { errorStack: err.stack });
        throw( err );
      });

    if( url ) {
        url_cache[qName] = url;
    }

    return url;
}

async function url_for_arn( arn ) {
    sqs_configure();
    const elements = arn.split(':');
    return await url_for_queue( elements[5] );
}

async function enq_standard( payload, attributes, q_url ) {
    sqs_configure();

    var params = {
        MessageAttributes: attributes,
        MessageBody: payload,
        QueueUrl: q_url
    };

    console.log( params );

    const sqsResponse = await sqs.sendMessage( params ).promise(); 
    return sqsResponse.MessageId;
}


async function delete_message( q_url, handle ) {
    sqs_configure();
    await sqs.deleteMessage( { QueueUrl : q_url, ReceiptHandle : handle}).promise();
}

module.exports = {
    url_for_queue,
    url_for_arn,
    delete_message,
    enq_standard
}
