const AWS = require('aws-sdk');
const uuid = require('uuid');

const {url_for_queue, enq_standard} = require( './sqs'); 

var queueURL = null;

async function coldstart() {
  if( !queueURL ) {
    console.log( "Coldstart" );
    console.log( `Q NAME:${process.env.QUEUE_NAME}`);
    queueURL = await url_for_queue( process.env.QUEUE_NAME );
    console.log( `Q URL:${queueURL}`);
  }
}

exports.handler = async (event) => {
  await coldstart();

  if (event.httpMethod !== 'POST') {
    throw new Error(`${event.httpMethod} not supported`);
  }

  console.info('received:', event);

  const payload = JSON.parse( event.body );
  payload.nuid = uuid.v4();
  await enq_standard( JSON.stringify(payload), {}, queueURL );
  
  const response = {
    statusCode: 200,
    body: JSON.stringify(payload)
  };
 
  console.info(`response from: ${event.path} statusCode: ${response.statusCode} body: ${response.body}`);
  return response;
};
