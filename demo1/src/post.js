
exports.handler = async (event) => {
  if (event.httpMethod !== 'POST') {
    throw new Error(`getMethod only accept POST method, you tried: ${event.httpMethod}`);
  }

  console.info('received: event', event);

  const payload = JSON.parse( event.body );
    
  const response = {
    statusCode: 200,
    body: JSON.stringify(payload)
  };
 
  console.info(`response from: ${event.path} statusCode: ${response.statusCode} body: ${response.body}`);
  return response;
};
